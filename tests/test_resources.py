# -*- coding: utf-8 -*-

import pytest
from tuxpub import resources
from conftest import bucket, region


@pytest.fixture
def s3_handle(mock_s3_client):
    """ Return an resources.s3_server handle """
    yield resources.s3_server(bucket, region)


@pytest.fixture
def s3_handle_101(mock_s3_client_101):
    """
    Return an resources.s3_server handle to a bucket which contains 101
    files under the 101/ prefix
    """
    yield resources.s3_server(bucket, region)


@pytest.mark.parametrize(
    "path,file_count,folder_count",
    [
        ("", 1, 2),
        ("build1/", 3, 1),
        ("build1/dtbs/", 4, 0),
        ("build2/", 4, 0),
        ("build2/bmeta.json", 1, 0),
        ("build2/empty_file", 0, 0),  # Empty objects are not returned
    ],
)
def test_get_files_folders_counts(path, file_count, folder_count, s3_handle):
    (files, folders, next_token) = s3_handle.get_files_and_folders(path)
    assert len(files) == file_count
    assert len(folders) == folder_count


@pytest.mark.parametrize(
    "key",
    [
        ("ipsum.txt"),
        ("build1/dtbs/zynq-zturn.dtb"),
        ("build1/build.log"),
        ("build2/dtbs.json"),
    ],
)
def test_create_signed_url(key, s3_handle):
    url = s3_handle.create_signed_url(key)
    assert url.startswith(f"https://{bucket}.s3.amazonaws.com/{key}")


@pytest.mark.parametrize(
    "key,exists",
    [
        ("ipsum.txt", True),
        ("build1/dtbs/zynq-zturn.dtb", True),
        ("build1/build.log", True),
        ("build2/dtbs.json", True),
        ("build1/empty_file", False),
        ("foo", False),
        ("build1/bar", False),
    ],
)
def test_key_exists(key, exists, s3_handle):
    assert s3_handle.key_exists(key) == exists


def test_get_files_and_folders_next_continuation_token(s3_handle_101):
    """ Test continuation_token handling in get_files_and_folders """
    (files, folders, next_token) = s3_handle_101.get_files_and_folders(
        "101/", max_keys=50
    )
    assert len(files) == 50
    (files, folders, next_token) = s3_handle_101.get_files_and_folders(
        "101/", continuation_token=next_token, max_keys=50
    )
    assert len(files) == 50
    (files, folders, next_token) = s3_handle_101.get_files_and_folders(
        "101/", continuation_token=next_token, max_keys=50
    )
    assert len(files) == 1
    assert next_token == ""
