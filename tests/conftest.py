# -*- coding: utf-8 -*-

"""
This file contains global fixtures which are available to all tests.
"""

import boto3
import os
import pytest
from moto.s3 import mock_s3

bucket = "tuxpub-example"
region = "us-west-1"
mock_s3_dir = os.path.join(os.path.dirname(__file__), "mock_s3")


@pytest.fixture
def aws_credentials(monkeypatch):
    """ Ensure no aws keys are available before shit gets real """
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"
    os.environ["S3_BUCKET"] = bucket
    os.environ["S3_REGION"] = region


@pytest.fixture
def mock_s3_client(aws_credentials):
    """
    Create a mock s3 client, and fill the fake bucket with the contents of
    the mock_s3 directory. Once this is loaded by a test (by including
    'mock_s3_client' argument), it will reply to any boto3 s3 calls
    automatically.
    """

    with mock_s3():
        client = boto3.client("s3", region_name=region)
        location = {"LocationConstraint": region}
        client.create_bucket(Bucket=bucket, CreateBucketConfiguration=location)
        for path, _, files in os.walk(mock_s3_dir):
            for filename in files:
                filepath = os.path.join(path, filename)
                key = os.path.relpath(filepath, mock_s3_dir)
                client.upload_file(Filename=filepath, Bucket=bucket, Key=key)
        yield client


@pytest.fixture
def mock_s3_client_101(aws_credentials):
    with mock_s3():
        client = boto3.client("s3", region_name=region)
        location = {"LocationConstraint": region}
        client.create_bucket(Bucket=bucket, CreateBucketConfiguration=location)
        for i in range(101):
            client.put_object(
                Body=f"101/{i}".encode("utf-8"), Bucket=bucket, Key=f"101/{i}"
            )
        yield client


def test_s3_fixture(mock_s3_client):
    """ Just make sure the s3 fixture works, and contains the stuff from mock_s3 """
    objects = mock_s3_client.list_objects_v2(Bucket=bucket)
    mock_file_count = sum([len(files) for r, d, files in os.walk(mock_s3_dir)])
    assert objects.get("KeyCount") == mock_file_count
